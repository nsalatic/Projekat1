﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer2
{
    /*
    Ime klase je isto kao u prethodnom primeru, 
    ali to je samo kraći naziv. Njen pun naziv je prostor imena + naziv klase
    Trudimo se da je puno ime klase jedinstveno ne samo na nivou 
    naseg projekta, nego na globalnom nivou - pogledajte naziv paketa 
    */
    class ArrayClass1
    {
        public static void Main(string[] args)
        {
            //naredni kod definise niz koji sadrzi prvih 5 prirodnih brojeva
            int[] A;            //deklaracija niza ciji su elementi celi brojevi
            A = new int[5];     //alokacija 5 memoriskih polja velicine celobrojnog tipa
            A[0] = 1;           //promena vrednosti 1. elementa niza
            A[1] = 2;           //promena vrednosti 2. elementa niza
            A[2] = 3;           //promena vrednosti 3. elementa niza
            A[3] = 4;           //promena vrednosti 4. elementa niza
            A[4] = 5;           //promena vrednosti 5. elementa niza

            //skraceni oblik ovog istog
            int[] B = { 1, 2, 3, 4, 5 };    //deklaracija, alokacija i inicijalizacija

            // elementi niza mogu da budu vrednost konstante, promenljive, rezultati poziva funkcije
            // niz realnih
            double[] Realni = { Math.PI / 2, 3.0, 2 + 3 / 5, Math.E, Math.Sqrt(3) };

            // niz reci
            String[] Imena = { "Pera", "Mika", "Laza" };

            //pristup elementu niza A
            Console.WriteLine("1. Peti element niza A je " + A[4] + "\n");

            //ispis niza pomocu metode ToString(), pogledajte rezult
            Console.WriteLine("2. Niz A je " + A + "\n");

            //velicina niza
            Console.WriteLine("3. Velicina niza A je " + A.Length);

            //ispis elemenata niza A
            Console.WriteLine("4. Ispis elemenata niza");
            for (int i = 0; i < A.Length; i++)
                Console.WriteLine(i + " clan niza A je " + A[i]);


            Console.ReadKey();
        }
    }
}
