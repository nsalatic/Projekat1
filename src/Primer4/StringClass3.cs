﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer4
{
    class StringClass3
    {
        public static void Main(String[] args)
        {

            //Sadrzaj csv fajla koji cuva podatke o studentima i njihovim ocenama
            String text = "Е1 01/16,Марио Николић,Лозница,Математика;9!Физика;7" + "\n" +
                                "Е1 02/16,Баки Страхиња,Нови Сад,Физика;7" + "\n" +
                                "Е1 03/15,Трајковић Небојша,Инђија,Математика;10" + "\n" +
                                "Е2 01/16,Синиша Николић,Ниш,Математика;9!Електроника;9!Информатика;10" + "\n" +
                                "Е2 02/15,Симић Вук,Нови Сад,Физика;5!Електроника;6" + "\n" +
                                "Е2 03/16,Калинић Марко,Сомбор,Електроника;5" + "\n" +
                                "Е2 04/15,Марко Панић,Шабац";


            //za prikaz naših ћириличних slova u konzoli, potrebno je podesiti odgovarajući format enkodovanja - UTF8
            Console.OutputEncoding = Encoding.UTF8;

            String[] sviRedovi = text.Split('\n');

            //popunjavanje podataka
            for (int i = 0; i < sviRedovi.Length; i++)
            {
                Console.WriteLine("\n************************************");
                Console.WriteLine("Red ->" + sviRedovi[i]);

                String[] studentPodaciTekst = sviRedovi[i].Split(',');
                Console.WriteLine("Preuzimanje i ispis podataka o studentu");

                String indeks = studentPodaciTekst[0];
                String imeiPrezime = studentPodaciTekst[1];
                String grad = studentPodaciTekst[2];

                Console.WriteLine("Student " + imeiPrezime + " sa indeksom " + indeks + " iz grada " + grad + " ima ocene");

                if (studentPodaciTekst.Length > 3)
                {
                    String[] studentSveOceneTekst = studentPodaciTekst[3].Split('!');

                    for (int j = 0; j < studentSveOceneTekst.Length; j++)
                    {
                        String[] studentOceneTekst = studentSveOceneTekst[j].Split(';');

                        String predmet = studentOceneTekst[0];
                        String ocenaTekst = studentOceneTekst[1];
                        Console.WriteLine("\tOcena iz " + predmet + " je " + ocenaTekst);

                        //int ocena = Int32.Parse(studentOceneTekst[1]);
                        //Console.WriteLine("\tOcena iz " + predmet + " je " + ocena);
                    }
                }

            }
            Console.WriteLine("\n************************************");
            Console.WriteLine("\nPrimer korišćenja StringBilder klase");

            StringBuilder sb = new StringBuilder("[ ");

            // kreiranje stringa brojeva
            for (int i = 0; i < 10; i++)
            {
                sb.Append(i.ToString()+" ");
                
            }
            sb.Append("]");

            System.Console.WriteLine(sb);  // Ispis: [ 0 1 2 3 4 5 6 7 8 9 ]

            // izmena stringa sto nije moguće sa klasom System.String
            sb[0] = sb[sb.Length-1] = '|';            

            System.Console.WriteLine(sb);  // Ispis: | 0 1 2 3 4 5 6 7 8 9 |
            Console.ReadKey();
        }
    }
}
